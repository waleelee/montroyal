
class config():
    word_dict_path = "./data/temp"
    document_words_num = 10
    query_words_num = 5
    memory_size = 100000
    batch_size = 1024
    doc_path = ['./data/oppo_round1_test_A_20180929.txt',]
    min_count = 0
    word_size =200
    neg_sample_size=10