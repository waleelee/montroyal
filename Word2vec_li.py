from config import config
import tensorflow as tf
import numpy as np
import jieba
from collections import Counter
import ipdb
import datetime
import random


class word2vec(object):

    def __init__(self,config):
        self.config = config
        self.memory = []
    def read_data(self):
        # 预处理文本得到词典。
        self.training_data = {}
        for docs in self.config.doc_path:
            with open(docs) as f:
                lines = f.readlines()
                for ii in range(len(lines)):
                    line = lines[ii]
                    lineContent = line.split('\t')
                    cont = {}
                    for i in lineContent[1].strip("{").strip("}").split(', '):
                        ii = [v.strip("\"") for v in i.split(": ")]
                        try:
                            cont[ii[0]] = ii[1]
                        except:
                            # print(lineContent)
                            pass
                    self.training_data[lineContent[0]] = cont

    def word_count(self):
        q_content = ""
        d_content = ""
        for k, v in self.training_data.items():  # 保证qa_dict为词典类型
            q_content += k + "\n"
            for kk, vv in v.items():
                d_content += kk + " "
        self.seg_q_dic = Counter(jieba.cut(q_content, cut_all=False))
        print(len(self.seg_q_dic))
        seg_d_dic = Counter(jieba.cut(d_content, cut_all=False))
        seg_all_list = self.seg_q_dic
        seg_all_list.update(seg_d_dic)
        print(len(self.seg_q_dic),'----------------',len(seg_d_dic),'----------------',len(seg_all_list))
        self.words = {i: j for i, j in seg_all_list.items() if j >= self.config.min_count}  # 设置最低词频，低于最低词频的单词将不进行向量训练
        self.id2words = {i + 1: j for i, j in enumerate(self.words.keys())}  # id（1，2，3...）和words单词
        self.words2id = {j: i for i, j in self.id2words.items()}
        self.total_words = len(self.words)
        # print(self.words2id)

    def build_model(self):
        with tf.Graph().as_default():
            self._create_placeholder()
            self._create_inference()
            self._create_loss_function()
            self.sess = tf.Session()
            self.sess.run(tf.global_variables_initializer())
            self.saver = tf.train.Saver(max_to_keep=100)


    def _create_placeholder(self):
        self.target_words = tf.placeholder(tf.int32, shape=[None, self.config.document_words_num])
        self.target_dim = tf.placeholder(tf.float32,shape=[None])
        self.target_probability = tf.placeholder(tf.float32, shape=[None])
        self.input_words = tf.placeholder(tf.int32, shape=[None, self.config.query_words_num])
        self.input_dim = tf.placeholder(tf.float32, shape=[None])
        pass

    def _create_inference(self):
        zeros = tf.Variable(np.zeros((1,self.config.word_size)),dtype=tf.float32,trainable=False)
        self.embeddings = tf.Variable(tf.random_uniform([self.total_words, self.config.word_size], 0, 0.5),trainable=True)
        # self.normalized_embeddings = tf.nn.softmax(self.embeddings, dim=1)
        # self.normalized_embeddings = self.embeddings
        # ipdb.set_trace()
        self.weights = tf.concat([zeros, self.embeddings], axis=0)
        print((self.weights).shape)
        # tf.gather_nd()
        self.target_vecs = tf.reduce_sum(tf.nn.embedding_lookup(self.weights, self.target_words),1) # 128,10,200 128,200 dim:[128,1]
        # self.target_vecs = tf.divide(tf.reduce_sum(self.target_vecs,1),tf.expand_dims(self.target_dim,1)) # 128,200 dim:[128,1]


        # 输入单词及向量，输入为Query中的单词，输出为
        # ipdb.set_trace()
        self.input_vecs = tf.nn.embedding_lookup(self.weights, self.input_words) # 128,5,200
        # self.input_vecs = tf.divide(tf.reduce_sum(self.input_vecs,1),tf.expand_dims(self.input_dim,1)) # 128,200 dim:[128,1]
        self.input_vecs = tf.reduce_sum(self.input_vecs,1)
        # 对input_vecs和target_vecs进行求和处理
        # self.input_vecs = tf.expand_dims(tf.reduce_sum(self.input_vecs, 1), 1)
        # self.target_vecs = tf.expand_dims(tf.reduce_sum(self.target_vecs, 1), 1)
        self.sample_logits = tf.reduce_sum(tf.multiply(self.target_vecs,self.input_vecs),1)


    def _create_loss_function(self):
        # sampled_logits = tf.matmul(self.input_vecs, self.target_vecs, transpose_b=True)
        self.loss = tf.reduce_mean(
            tf.nn.sigmoid_cross_entropy_with_logits(logits=self.target_probability, labels=self.sample_logits))
        # self.loss = tf.reduce_sum(tf.squared_difference(self.sample_logits,self.target_probability))
        self.train_step = tf.train.AdadeltaOptimizer(learning_rate=0.01).minimize(self.loss)

    def data_generator(self):
        for idx,t in self.training_data.items():#idx是query，t是一个词典，包含document以及probability
            query_words_list = jieba.cut(idx,cut_all=False)
            x,y,x_dim,y_dim = [],[],[],[]
            tempy=[self.words2id[word] for word in query_words_list]
            y_dim.append(len(tempy))
            tempy+=[0]*(self.config.query_words_num-len(tempy))
            p=[]
            for kk,vv in t.items():#t是一个词典，格式为{document:probability}
                document_words_list = jieba.cut(kk,cut_all=False)

                winx=[self.words2id[word] for word in document_words_list]#存储Document中的单词list
                x_dim.append(len(winx))
                winx += [0] * (self.config.document_words_num-len(winx))
                neg_sample_size = len(winx)
                x.append(winx)#positive
                y.append(tempy)

                p.append(1. if float(vv) > 0.1 else 0.)
                if len(x)>(self.config.batch_size):
                    # x.append(x)  # negative
                    # y.append(np.random.randint(1, self.total_words, [len(x),self.config.document_words_num]))
                    yield x,x_dim,y,y_dim,p
            if x:
                # x.append(x)  # negative
                # y.append(np.random.randint(0, self.total_words, [len(x), self.config.document_words_num]))
                yield x,x_dim,y,y_dim,p

    def train(self):
        # load data
        epoch = 0
        while epoch<10:
            count = 0
            for x,x_dim,y,y_dim,p in self.data_generator():
                temp = zip(x,x_dim,y,y_dim,p)
                self.memory.extend(temp)
                if len(self.memory)>self.config.batch_size:
                    index = np.random.choice(list(range(len(self.memory))),size=(self.config.batch_size,))
                    batch = np.asarray(self.memory)[index]
                    try:
                        temp_dict = {self.input_words:[item[2] for item in batch],
                                                                    self.input_dim:[item[3] for item in batch],
                                                                    self.target_words:[item[0] for item in batch],
                                                                    self.target_dim:[item[1] for item in batch],
                                                                    self.target_probability:[item[4] for item in batch]}
                        ipdb.set_trace()
                        _,loss_ = self.sess.run([self.train_step,self.loss], feed_dict=temp_dict)
                        if count % 10 == 0:
                            print(
                                '%s, articles, loss: %s' % (datetime.datetime.now(), loss_))
                    except:
                        # ipdb.set_trace()
                        pass
                    count += 1
                if len(self.memory)>self.config.memory_size:
                    self.memory = self.memory[len(self.memory)-self.config.memory_size:]
            epoch+=1
        #
        pass

model =word2vec(config=config)
model.read_data()
model.word_count()
model.build_model()
model.train()